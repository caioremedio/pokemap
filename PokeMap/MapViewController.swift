//
//  MapViewController.swift
//  PokeMap
//
//  Created by Caio Remedio on 17/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import MapKit

let kPokemonPinIdentifier = "pokemonPinIdentifier"
let kFastPokemapPinRadius: CLLocationDistance = CLLocationDistance(120)

class MapViewController: UIViewController {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var lblPokeRadarStatus: UILabel!
    @IBOutlet var lblFastPokeMapStatus: UILabel!
    @IBOutlet var btnClear: UIButton!
    @IBOutlet var btnCurrentLocation: UIButton!

    var pokemons = [PMPokemon]()
    var mapCornersLocation: MapCornersLocation?
    var isPerformingRequest: Bool = false
    var isPerformingFastPokemapRequest: Bool = false
    var longPressGestureRecognizer: UILongPressGestureRecognizer?
    var previousPinAnnotation: MKPointAnnotation?
    var isUserLocationVisible: Bool = false

    var timerForRequest: NSTimer?

    init() {
        super.init(nibName: String(MapViewController), bundle: nil)

        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(locationManagerDidFindLocationNotification),
                                                         name: LocationManagerDidChangeLocationStatusNotification,
                                                         object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(locationManagerDidChangeLocationStatusNotification),
                                                         name: LocationManagerDidChangeLocationStatusNotification,
                                                         object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(pokemonAnnotationViewDidExpireNotification),
                                                         name: PokemonAnnotationViewDidExpireNotification,
                                                         object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: String(MapViewController), bundle: nil)
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        unassignTimer()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeMapView()

        btnCurrentLocation.layer.borderColor = UIColor.blackColor().CGColor
        btnCurrentLocation.layer.borderWidth = 0.5
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        assignTimer()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        unassignTimer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func assignTimer() {
        timerForRequest = NSTimer.scheduledTimerWithTimeInterval(15,
                                                       target: self,
                                                       selector: #selector(timerTask),
                                                       userInfo: nil,
                                                       repeats: true)
    }

    func initializeMapView() {
        LocationManager.sharedInstance.startUpdatingLocation()

        let userLocation = LocationManager.userLocation ?? CLLocation(latitude: -23.546291,
                                                                      longitude: -46.641147)

        let userRegion = MKCoordinateRegion(center: userLocation.coordinate,
                                            span: MKCoordinateSpanMake(0.012, 0.012))
        mapView.setRegion(userRegion, animated: false)

        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self,
                                                                  action: #selector(mapViewLongPressGestureAction))
        longPressGestureRecognizer?.minimumPressDuration = 1
        mapView.addGestureRecognizer(longPressGestureRecognizer!)
    }

    func unassignTimer() {
        timerForRequest?.invalidate()
        timerForRequest = nil
    }

    func timerTask() {
        requestData()
    }

    func requestFastPokeMapData() {

        if isPerformingFastPokemapRequest { return }

        isPerformingFastPokemapRequest = true
        self.updateLabelStatus()

        var requestCoordinate = CLLocationCoordinate2D()

        if let previousPinAnnotation = previousPinAnnotation {
            requestCoordinate = previousPinAnnotation.coordinate
        } else if let userLocation = LocationManager.sharedInstance.userLocation {
            requestCoordinate = userLocation.coordinate
        } else {
            print("\(#function) - There is no location available for FastPokeMap")
            self.isPerformingFastPokemapRequest = false
            self.updateLabelStatus()
            return
        }

        FastPokemapHelper.pokemonsFromCoordinate(requestCoordinate) { (pokemons, succeeded) in
            guard let pokemons = pokemons where succeeded else {
                self.isPerformingRequest = false
                print("\(#function) - Request Failed. Trying again")
                self.isPerformingFastPokemapRequest = false
                self.requestFastPokeMapData()
                return
            }

            print("\(#function) - Found FastPokeMap \(pokemons.count) pokemons")

            if pokemons.count == 0 {
                print("\(#function) - FastPokeMap - Nothing was found!")
                self.isPerformingFastPokemapRequest = false
                self.updateLabelStatus()
                return
            }

            var newPokemons = [PMPokemon]()

            pokemons.forEach({
                let pokemon = $0
                var shouldSkipPokemon = false

                self.pokemons.forEach({
                    if $0 == pokemon {
                        shouldSkipPokemon = true
                        return
                    }
                })

                if !shouldSkipPokemon {
                    newPokemons.append($0)
                }
            })

            self.pokemons.appendContentsOf(newPokemons)

            self.isPerformingFastPokemapRequest = false
            self.updateLabelStatus()
            self.mapView.addAnnotations(newPokemons)
        }
    }

    func requestData() {

        if isPerformingRequest { return }

        isPerformingRequest = true
        self.updateLabelStatus()

        mapCornersLocation = mapView.cornersLocation()
        let maxLocation = mapCornersLocation!.northeastCoordinate
        let minLocation = mapCornersLocation!.southwestCoordinate

        PokeradarHelper.pokemonsFromMaxLocation(maxLocation, minLocation: minLocation) { (pokemons, succeeded) in

            guard let pokemons = pokemons where succeeded else {
                self.isPerformingRequest = false
                print("\(#function) - Request Failed. Trying again")
//                self.requestData()
                return
            }

            print("\(#function) - Found PokeRadar \(pokemons.count) pokemons")

            if pokemons.count == 0 {
                print("\(#function) - PokeRadar - Nothing was found!")
                self.isPerformingRequest = false
                self.updateLabelStatus()
                return
            }

            var newPokemons = [PMPokemon]()

            pokemons.forEach({
                let pokemon = $0
                var shouldSkipPokemon = false

                self.pokemons.forEach({
                    if $0 == pokemon {
                        shouldSkipPokemon = true
                        return
                    }
                })

                if !shouldSkipPokemon {
                    newPokemons.append($0)
                }
            })

            self.pokemons.appendContentsOf(newPokemons)

            self.isPerformingRequest = false
            self.updateLabelStatus()
            self.mapView.addAnnotations(newPokemons)
        }

        if let userCoordinate = LocationManager.sharedInstance.userLocation?.coordinate {
            FastPokemapHelper.pokemonsCachedFromCoordinate(userCoordinate) { (pokemons, succeeded) in

                guard let pokemons = pokemons where succeeded else {
                    print("\(#function) - Request Failed. Trying again")
                    return
                }

                print("\(#function) - Found FastPokeMap Cache \(pokemons.count) pokemons")

                if pokemons.count == 0 {
                    print("\(#function) - PokeRadar - Nothing was found!")
                    return
                }

                var newPokemons = [PMPokemon]()

                pokemons.forEach({
                    let pokemon = $0
                    var shouldSkipPokemon = false

                    self.pokemons.forEach({
                        if $0 == pokemon {
                            shouldSkipPokemon = true
                            return
                        }
                    })

                    if !shouldSkipPokemon {
                        newPokemons.append(pokemon)
                    }
                })
                
                self.pokemons.appendContentsOf(newPokemons)
                self.mapView.addAnnotations(newPokemons)
            }
        }
    }

    func locationManagerDidFindLocationNotification(notification: NSNotification) {

        guard let userLocationCoordinate = LocationManager.userLocation?.coordinate else { return }

        let userRegion = MKCoordinateRegion(center: userLocationCoordinate,
                                            span: MKCoordinateSpanMake(0.2, 0.2))
        mapView.setRegion(userRegion, animated: true)
    }

    func locationManagerDidChangeLocationStatusNotification(notification: NSNotification) {

    }

    func mapViewLongPressGestureAction(sender: UILongPressGestureRecognizer) {

        if sender.state != .Began { return }

        let touchPoint = sender.locationInView(mapView)
        let touchCoordinate = mapView.convertPoint(touchPoint, toCoordinateFromView: mapView)

        if let previousPinAnnotation = previousPinAnnotation {
            mapView.removeAnnotation(previousPinAnnotation)
        }

        let customAnnotation = MKPointAnnotation()
        customAnnotation.coordinate = touchCoordinate
        self.previousPinAnnotation = customAnnotation

        mapView.addAnnotation(customAnnotation)
        addCircleAtCoordinate(customAnnotation.coordinate)

        requestFastPokeMapData()
    }

    func addCircleAtCoordinate(coordinate: CLLocationCoordinate2D) {
        mapView.removeOverlays(mapView.overlays)
        let mkCircle = MKCircle(centerCoordinate: coordinate, radius: kFastPokemapPinRadius)
        mapView.addOverlay(mkCircle)
    }

    func updateLabelStatus() {

        lblPokeRadarStatus.text = isPerformingRequest ? "PokeRadar: L" : "PokeRadar: D"
        lblFastPokeMapStatus.text = isPerformingFastPokemapRequest ? "FastPokeMap: L" : "FastPokeMap: D"
    }

    @IBAction func btnClearTap(sender: AnyObject) {
        mapView.removeAnnotations(mapView.annotations)
        pokemons.removeAll()
    }

    @IBAction func btnShowLocationTap(sender: AnyObject) {

        guard let userLocation = LocationManager.sharedInstance.userLocation else { return }

        let userRegion = MKCoordinateRegion(center: userLocation.coordinate,
                                            span: MKCoordinateSpanMake(0.01, 0.01))
        mapView.setRegion(userRegion, animated: true)
    }

    func pokemonAnnotationViewDidExpireNotification(notification: NSNotification) {
        if let pokemonAnnotationView = notification.object as? PokemonAnnotationView,
            annotationView = pokemonAnnotationView.annotation,
            indexOfPokemon = pokemons.indexOf(annotationView as! PMPokemon) {
            mapView.removeAnnotation(annotationView)
            pokemons.removeAtIndex(indexOfPokemon)
        }
    }

}
