//
//  MapViewController+MapView.swift
//  PokeMap
//
//  Created by Caio Remedio on 17/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import Foundation
import MapKit

extension MapViewController: MKMapViewDelegate {

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {

        if annotation is MKUserLocation { return nil }

        var pokemonAnnotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(kPokemonPinIdentifier)
            as? PokemonAnnotationView

        guard let annotation = annotation as? PMPokemon else {
            let defaultPinAnnotation = MKPinAnnotationView(annotation: nil,
                                                           reuseIdentifier: "default_pin")
            defaultPinAnnotation.animatesDrop = true
            defaultPinAnnotation.canShowCallout = true
            defaultPinAnnotation.leftCalloutAccessoryView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            return defaultPinAnnotation
        }

        if pokemonAnnotationView == nil {
            pokemonAnnotationView = PokemonAnnotationView(annotation: annotation,
                                                          reuseIdentifier: kPokemonPinIdentifier)
        }

        pokemonAnnotationView?.image = annotation.imgIcon

        return pokemonAnnotationView
    }

    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {

        if self.isUserLocationVisible { return }

        self.isUserLocationVisible = true
        
        let userRegion = MKCoordinateRegion(center: userLocation.coordinate,
                                            span: MKCoordinateSpanMake(0.01, 0.01))
        mapView.setRegion(userRegion, animated: true)
    }

    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {

        let newMapCornersLocation = mapView.cornersLocation()

        if mapCornersLocation == nil || mapCornersLocation != newMapCornersLocation {
            requestData()
        }
    }

    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {

        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.blueColor()
            circle.fillColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.2)
            circle.lineWidth = 1
            return circle
        }

        return MKOverlayRenderer()
    }
}