//
//  PokeradarHelper.swift
//  PokeMap
//
//  Created by Caio Remedio on 18/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import Alamofire
import SwiftyJSON

class PokeradarHelper {

    static let kPokeradarBaseUrl = "https://www.pokeradar.io/api/v1/submissions"

    static func pokemonsFromMaxLocation(maxLocation: CLLocationCoordinate2D,
                                        minLocation: CLLocationCoordinate2D,
                                        completion: (pokemons: [PMPokemon]?, succeeded: Bool) -> Void ) {

        var url = kPokeradarBaseUrl
        url += "?deviceId=08df57a064e211e6b876dd5b54347aef"
        url += "&minLatitude=\(minLocation.latitude)"
        url += "&maxLatitude=\(maxLocation.latitude)"
        url += "&minLongitude=\(minLocation.longitude)"
        url += "&maxLongitude=\(maxLocation.longitude)"
        url += "&pokemonId=0"

        print("\(#function) - Requesting PokeRadar Data:\n\(url.debugDescription)")

        Alamofire.request(.GET, url)
        .validate()
        .responseSwiftyJSON { (request, response, json, errorType) in
            guard let data = json["data"].array
                where errorType == nil &&
                    json["success"].boolValue &&
                    json["errors"].arrayValue.isEmpty
                else { return completion(pokemons: nil, succeeded: false) }


            //
            //  If nothing was found
            //
            if data.count == 0 { return completion(pokemons: [], succeeded: true) }

            //
            // Found some pokemons
            //

            var pokemons = [PMPokemon]()
            let count = data.count > 300 ? 300 : data.count - 1

            for i in 0...count {
                let pokemonJSON = data[i]
                if pokemonJSON["trainerName"].stringValue == "(Poke Radar Prediction)" {
                    pokemons.append(PMPokemon(pokeRadarJSON: pokemonJSON) ?? PMPokemon())
                }
            }

            return completion(pokemons: pokemons, succeeded: true)
        }
    }
}
