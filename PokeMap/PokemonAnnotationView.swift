//
//  PokemonAnnotationView.swift
//  PokeMap
//
//  Created by Caio Remedio on 17/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import MapKit

let PokemonAnnotationViewDidExpireNotification = "pokemonAnnotationViewDidExpire"

class PokemonAnnotationView: MKAnnotationView {

    var lblDuration: UILabel?
    var pokemon: PMPokemon?
    var timerForPinUpdate: NSTimer?

    override init(annotation: MKAnnotation!, reuseIdentifier: String!) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.centerOffset = CGPointMake(0.5, 1.0)
        self.canShowCallout = true
        if let pokemon = annotation as? PMPokemon {
            self.pokemon = pokemon
            self.image = pokemon.imgIcon
            self.lblDuration = UILabel(frame: CGRectMake(0, (self.frame.height-10) - self.frame.height/2, self.frame.width, 20))
            self.lblDuration?.text = stringForExpirationTimeLeft(DateHelper.sharedInstance.timeLeftFromDate(pokemon.expirationDate))
            self.lblDuration?.textAlignment = .Center
            self.addSubview(self.lblDuration!)

            if self.timerForPinUpdate == nil {
                self.timerForPinUpdate = NSTimer.scheduledTimerWithTimeInterval(1,
                                                                                target: self,
                                                                                selector: #selector(updateExpirationTimeLeft),
                                                                                userInfo: nil,
                                                                                repeats: true)
            }
            
        }
    }
    
    override init(frame: CGRect) { super.init(frame: frame) }

    required init(coder aDecoder: NSCoder) { super.init(coder: aDecoder)! }

    func updateExpirationTimeLeft() {
        if let pokemon = annotation as? PMPokemon {
            let timeLeftText = DateHelper.sharedInstance.timeLeftFromDate(pokemon.expirationDate)

            if timeLeftText.0 <= 0 && timeLeftText.1 < 0 {
                NSNotificationCenter.defaultCenter().postNotificationName(PokemonAnnotationViewDidExpireNotification,
                                                                          object: self)
            }

            lblDuration?.text = stringForExpirationTimeLeft(timeLeftText)
        }
    }

    func stringForExpirationTimeLeft(timeLeft: (Int, Int)) -> String {
        return (timeLeft.0 == 0 && timeLeft.1 == 0) ? "?" : "\(timeLeft.0):\(timeLeft.1)"
    }
}
