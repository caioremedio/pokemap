//
//  FastPokemapHelper.swift
//  PokeMap
//
//  Created by Caio Remedio on 22/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

class FastPokemapHelper: NSObject {

    static let kFastPokemapBaseUrl = "https://api.fastpokemap.se/"
    static let kFastPokemapCacheBaseUrl = "https://cache.fastpokemap.se/"

    static func pokemonsFromCoordinate(coordinate: CLLocationCoordinate2D,
                                       completion: (pokemons: [PMPokemon]?, succeeded: Bool) -> Void ) {

        if coordinate.latitude == 0 ||
            coordinate.longitude == 0 { return completion(pokemons: nil, succeeded: false) }

        var url = kFastPokemapBaseUrl
        url += "?lat=\(coordinate.latitude)"
        url += "&lng=\(coordinate.longitude)"

        print("\(#function) - Requesting FastPokemap Data:\n\(url.debugDescription)")

        Alamofire.request(.GET,
            url,
            parameters: nil,
            encoding: .URLEncodedInURL,
            headers: ["Origin": "https://fastpokemap.se"])
            .validate()
            .responseSwiftyJSON { (request, response, json, errorType) in
                print("\(#function) - Response: \(json.debugDescription)")
                guard let data = json["result"].array
                    where errorType == nil &&
                        json["error"].string == nil
                    else { return completion(pokemons: nil, succeeded: false) }

                //
                //  It can be an error or it there is nothing around. Lets make it as suceeded
                //

                if data.count == 0 { return completion(pokemons: nil, succeeded: true) }

                var pokemons = [PMPokemon]()
                let count = data.count > 300 ? 300 : data.count - 1

                for i in 0...count {
                    let pokemon = data[i]

                    //
                    //  If there is no latitude/longitude key, because it's from Nearby. Lets just skip it for now.
                    //

                    if pokemon["latitude"].double == nil || pokemon["longitude"].double == nil { continue }

                    pokemons.append(PMPokemon(fastPokeMapJSON: pokemon) ?? PMPokemon())
                }

                return completion(pokemons: pokemons, succeeded: true)
        }
    }

    static func pokemonsCachedFromCoordinate(coordinate: CLLocationCoordinate2D,
                                             completion: (pokemons: [PMPokemon]?, succeeded: Bool) -> Void ) {

        if coordinate.latitude == 0 ||
            coordinate.longitude == 0 { return completion(pokemons: nil, succeeded: false) }

        var url = kFastPokemapCacheBaseUrl
        url += "?key=allow-all"
        url += "&ts=0"
        url += "&lat=\(coordinate.latitude)"
        url += "&lng=\(coordinate.longitude)"

        print("\(#function) - Requesting FastPokemap CACHE Data:\n\(url.debugDescription)")

        Alamofire.request(.GET,
            url,
            parameters: nil,
            encoding: .URLEncodedInURL,
            headers: ["Origin": "https://fastpokemap.se"])
            .validate()
            .responseSwiftyJSON { (request, response, json, errorType) in
//                print("\(#function) - Response: \(json.debugDescription)")

                if errorType != nil { return completion(pokemons: nil, succeeded: false) }

                //
                //  It can be an error or it there is nothing around. Lets make it as suceeded
                //

                if json.count == 0 { return completion(pokemons: nil, succeeded: true) }

                var pokemons = [PMPokemon]()
                let count = json.count > 300 ? 300 : json.count - 1

                for i in 0...count {
                    let pokemon = json[i]
                    pokemons.append(PMPokemon(fastPokeMapCacheJSON: pokemon) ?? PMPokemon())
                }
                
                return completion(pokemons: pokemons, succeeded: true)
        }
    }
}
