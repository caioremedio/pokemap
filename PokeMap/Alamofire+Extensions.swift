//
//  Alamofire+Extensions.swift
//  Delivery Direto
//
//  Created by Caio Remedio on 6/1/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let MaxNumberForRetries = 10

extension Alamofire.Request {

    func responseSwiftyJSON(
        options: NSJSONReadingOptions = .AllowFragments,
        completionHandler: (NSURLRequest, NSHTTPURLResponse?, JSON, ErrorType?) -> Void) -> Self {

        return response(
            queue: dispatch_get_main_queue(),
            responseSerializer: Request.JSONResponseSerializer(options: options),
            completionHandler: { response in

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    var switfyJSON: JSON
                    if response.result.isFailure {
                        switfyJSON = JSON.null
                    }
                    else {
                        switfyJSON = SwiftyJSON.JSON(response.result.value!)
                    }

                    dispatch_async(dispatch_get_main_queue(), {
                        completionHandler(
                            response.request!,
                            response.response,
                            switfyJSON,
                            response.result.error)
                    })
                })
        })
    }
}
