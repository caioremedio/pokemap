//
//  DateHelper.swift
//  PokeMap
//
//  Created by Caio Remedio on 27/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit

class DateHelper: NSObject {

    //-----------------------------------------------------------------------------
    // MARK: - Singleton
    //-----------------------------------------------------------------------------

    static let sharedInstance = DateHelper()

    //-----------------------------------------------------------------------------
    // MARK: - Properties
    //-----------------------------------------------------------------------------

    var calendar: NSCalendar
    var units: NSCalendarUnit
    var dateFormatter: NSDateFormatter
    var customDateFormatter: NSDateFormatter
    var localTimeZone: NSTimeZone
    var dateFormatterFastPokemap: NSDateFormatter

    //-----------------------------------------------------------------------------
    // MARK: - Initialization
    //-----------------------------------------------------------------------------

    private override init() {
        self.calendar = NSCalendar.currentCalendar()
        self.units = [.Year, .Month, .Weekday, .Day, .Hour, .Minute, .Second]
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter.dateStyle = .ShortStyle
        self.customDateFormatter = NSDateFormatter()
        self.localTimeZone = NSTimeZone.defaultTimeZone()
        self.dateFormatterFastPokemap = NSDateFormatter()
        self.dateFormatterFastPokemap.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        self.dateFormatterFastPokemap.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
//        self.dateFormatterFastPokemap.timeZone = self.localTimeZone
    }

    //-----------------------------------------------------------------------------
    // MARK: - Public Functions
    //-----------------------------------------------------------------------------

    func isStringDateValid(dateString: String) -> Bool {
        return dateFormatter.dateFromString(dateString) != nil
    }

    ///  Create an NSDate object using user's locale short format. IE:(dd/mm/yyyy)
    ///
    ///  - parameter string: Input String
    ///
    ///  - returns: NSDate or nil (if is invalid)
    func dateFromString(string: String) -> NSDate? {
        return dateFormatter.dateFromString(string)
    }

    func dateFromFastPokemapString(string: String) -> NSDate? {
        return dateFormatterFastPokemap.dateFromString(string)
    }

    func stringFromDate(date: NSDate?) -> String {
        guard let date = date else { return "" }

        return dateFormatter.stringFromDate(date)
    }

    func stringFromDate(date: NSDate?, withDateFormat dateFormat: String) -> String {
        guard let date = date else { return "" }

        customDateFormatter.dateFormat = dateFormat
        return customDateFormatter.stringFromDate(date)
    }

    func hourFromDate(date: NSDate?, is24Format: Bool) -> String {
        guard let date = date else { return "" }
        customDateFormatter.dateFormat = is24Format ? "H" : "h"
        return customDateFormatter.stringFromDate(date)
    }

    func minuteFromDate(date: NSDate?) -> String {
        guard let date = date else { return "" }
        customDateFormatter.dateFormat = "m"
        return customDateFormatter.stringFromDate(date)
    }

    func secondFromDate(date: NSDate?) -> String {
        guard let date = date else { return "" }
        customDateFormatter.dateFormat = "s"
        return customDateFormatter.stringFromDate(date)
    }

    func timeLeftFromDate(date: NSDate?) -> (Int, Int) {
        guard let date = date else { return (0,0) }

        let nowInterval = NSDate().timeIntervalSince1970 * 1000
        let pokemonExpirationInterval = date.timeIntervalSince1970 * 1000
        let timeLeftInterval = (pokemonExpirationInterval - nowInterval)
        
        return (Int(timeLeftInterval/60000%60), Int(timeLeftInterval/1000%60))
    }
}
