//
//  LocationManager.swift
//  PokeMap
//
//  Created by Caio Remedio on 20/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import MapKit

let LocationManagerDidFindLocationNotification = "locationManagerDidFindLocation"
let LocationManagerDidChangeLocationStatusNotification = "locationManagerDidChangeLocationStatus"

class LocationManager: NSObject {

    static let sharedInstance = LocationManager()

    var locationManager: CLLocationManager
    var userLocation: CLLocation?
    let desiredAccuracy: CLLocationAccuracy

    static let userLocation: CLLocation? = {
        return LocationManager.sharedInstance.userLocation
    }()

    static let userLatitude: Float = {
        return Float(LocationManager.sharedInstance.userLocation?.coordinate.latitude ?? 0)
    }()

    static let userLongitude: Float = {
        return Float(LocationManager.sharedInstance.userLocation?.coordinate.longitude ?? 0)
    }()

    private override init() {
        self.locationManager = CLLocationManager()
        self.desiredAccuracy = 150

        super.init()

        self.locationManager.stopMonitoringSignificantLocationChanges()
        self.locationManager.delegate = self
    }

    func startUpdatingLocation() {
        let authStatus = CLLocationManager.authorizationStatus()

        if authStatus == .NotDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }

        locationManager.startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {

        print("\(#function) - Location Status changed: \(status)")

        if status == .AuthorizedWhenInUse || status == .AuthorizedAlways {
            startUpdatingLocation()
        }

        NSNotificationCenter.defaultCenter().postNotificationName(LocationManagerDidChangeLocationStatusNotification,
                                                                  object: self)
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let newLocation = locations.last
            where newLocation.coordinate.latitude != 0.0 && newLocation.coordinate.longitude != 0.0 else {
                print("\(#function) - Invalid new location \(locations.last.debugDescription)")
                return
        }

        if (newLocation.timestamp.timeIntervalSinceNow < -30.0) {
            print("\(#function) - Location is old \(locations.last.debugDescription)")
            return
        }

        if (newLocation.horizontalAccuracy > self.desiredAccuracy) {
            print("\(#function) - Location accuracy is worse: \(newLocation.horizontalAccuracy)")
            return
        }

        print("\(#function) - Location accpeted!")

        userLocation = newLocation

        NSNotificationCenter.defaultCenter().postNotificationName(LocationManagerDidFindLocationNotification,
                                                                  object: self)

        //
        // Stop updating location if is accurate
        //

        if newLocation.horizontalAccuracy <= 30 {
            print("\(#function) - Location accuracy is ok, stopping updating!")
            locationManager.stopUpdatingLocation()
        }
    }
}
