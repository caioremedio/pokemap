//
//  PMPokemon.swift
//  PokeMap
//
//  Created by Caio Remedio on 17/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import MapKit
import Foundation
import SwiftyJSON

let pokemonList = ["All","Bulbasaur","Ivysaur","Venusaur","Charmander","Charmeleon","Charizard","Squirtle","Wartortle","Blastoise","Caterpie","Metapod","Butterfree","Weedle","Kakuna","Beedrill","Pidgey","Pidgeotto","Pidgeot","Rattata","Raticate","Spearow","Fearow","Ekans","Arbok","Pikachu","Raichu","Sandshrew","Sandslash","Nidoran♀","Nidorina","Nidoqueen","Nidoran♂","Nidorino","Nidoking","Clefairy","Clefable","Vulpix","Ninetales","Jigglypuff","Wigglytuff","Zubat","Golbat","Oddish","Gloom","Vileplume","Paras","Parasect","Venonat","Venomoth","Diglett","Dugtrio","Meowth","Persian","Psyduck","Golduck","Mankey","Primeape","Growlithe","Arcanine","Poliwag","Poliwhirl","Poliwrath","Abra","Kadabra","Alakazam","Machop","Machoke","Machamp","Bellsprout","Weepinbell","Victreebel","Tentacool","Tentacruel","Geodude","Graveler","Golem","Ponyta","Rapidash","Slowpoke","Slowbro","Magnemite","Magneton","Farfetch'd","Doduo","Dodrio","Seel","Dewgong","Grimer","Muk","Shellder","Cloyster","Gastly","Haunter","Gengar","Onix","Drowzee","Hypno","Krabby","Kingler","Voltorb","Electrode","Exeggcute","Exeggutor","Cubone","Marowak","Hitmonlee","Hitmonchan","Lickitung","Koffing","Weezing","Rhyhorn","Rhydon","Chansey","Tangela","Kangaskhan","Horsea","Seadra","Goldeen","Seaking","Staryu","Starmie","Mr. Mime","Scyther","Jynx","Electabuzz","Magmar","Pinsir","Tauros","Magikarp","Gyarados","Lapras","Ditto","Eevee","Vaporeon","Jolteon","Flareon","Porygon","Omanyte","Omastar","Kabuto","Kabutops","Aerodactyl","Snorlax","Articuno","Zapdos","Moltres","Dratini","Dragonair","Dragonite","Mewtwo","Mew"]

class PMPokemon: NSObject, MKAnnotation {

    var id: Int?
    var imgIcon: UIImage?
    var expirationDate: NSDate?
    var latitude: Double?
    var longitude: Double?

    lazy var coordinate: CLLocationCoordinate2D = {
        return CLLocation(latitude: self.latitude ?? 0,
                          longitude: self.longitude ?? 0).coordinate
    }()

    var title: String? {
        return pokemonList[self.id ?? 0]
    }

    override init() { }

    init(id: Int, expirationDate: NSDate, latitude: Double, longitude: Double) {
        self.id = id
        self.expirationDate = expirationDate
        self.latitude = latitude
        self.longitude = longitude
        self.imgIcon = UIImage(named: String(id))
        super.init()
    }
    
    convenience init?(pokeRadarJSON: JSON) {
        guard !pokeRadarJSON.isEmpty else { return nil }
        self.init()
        
        self.id = pokeRadarJSON["pokemonId"].int
        self.latitude = pokeRadarJSON["latitude"].double
        self.longitude = pokeRadarJSON["longitude"].double
        self.imgIcon = UIImage(named: String(id ?? 0))

        let expireAt = pokeRadarJSON["created"].doubleValue + 900 // 15 minutes

        self.expirationDate = NSDate(timeIntervalSince1970: expireAt)
    }

    convenience init?(fastPokeMapJSON: JSON) {
        guard !fastPokeMapJSON.isEmpty else { return nil }
        self.init()

        //
        //  Pokemon found on Lure
        //

        if let pokemonFromLure = fastPokeMapJSON["lure_info"].dictionary {
            self.id = pokemonList.indexOf(pokemonFromLure["active_pokemon_id"]?.stringValue.capitalizedString ?? "All")
            self.expirationDate = NSDate(timeIntervalSince1970:
                Double(pokemonFromLure["lure_expires_timestamp_ms"]?.stringValue ?? "0")!/1000)
        } else {
            self.id = pokemonList.indexOf(fastPokeMapJSON["pokemon_id"].stringValue.capitalizedString)
            self.expirationDate = NSDate(timeIntervalSince1970:
                Double(fastPokeMapJSON["expiration_timestamp_ms"].stringValue)!/1000)
        }

        self.latitude = fastPokeMapJSON["latitude"].double
        self.longitude = fastPokeMapJSON["longitude"].double
        self.imgIcon = UIImage(named: String(id ?? 0))
    }

    convenience init?(fastPokeMapCacheJSON: JSON) {
        guard !fastPokeMapCacheJSON.isEmpty else { return nil }
        self.init()

        self.id = pokemonList.indexOf(fastPokeMapCacheJSON["pokemon_id"].stringValue.capitalizedString)

        if let expireAt = fastPokeMapCacheJSON["expireAt"].string where !expireAt.isEmpty {
            self.expirationDate = DateHelper.sharedInstance.dateFromFastPokemapString(expireAt)
        }

        if let coordinates = fastPokeMapCacheJSON["lnglat"]["coordinates"].array where coordinates.count >= 2 {
            self.longitude = coordinates[0].double
            self.latitude = coordinates[1].double
        }

        self.imgIcon = UIImage(named: String(id ?? 0))
    }
}

// MARK: Equatable

func ==(lhs: PMPokemon, rhs: PMPokemon) -> Bool {
    return lhs.id == rhs.id &&
        lhs.latitude == rhs.latitude &&
        lhs.longitude == rhs.longitude
}

