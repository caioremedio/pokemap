//
//  MapHelper.swift
//  PokeMap
//
//  Created by Caio Remedio on 20/08/16.
//  Copyright © 2016 PokeMap. All rights reserved.
//

import UIKit
import MapKit

struct MapCornersLocation {
    let northeastCoordinate: CLLocationCoordinate2D
    let southwestCoordinate: CLLocationCoordinate2D

    init(northeastCoorinate: CLLocationCoordinate2D,
         southwestCoordinate: CLLocationCoordinate2D) {

        self.northeastCoordinate = northeastCoorinate
        self.southwestCoordinate = southwestCoordinate
    }
}

extension MapCornersLocation: Equatable { }

func ==(lhs: MapCornersLocation, rhs: MapCornersLocation) -> Bool {
    return lhs.northeastCoordinate.longitude == rhs.northeastCoordinate.longitude &&
         lhs.northeastCoordinate.latitude == rhs.northeastCoordinate.latitude &&
        lhs.southwestCoordinate.longitude == rhs.southwestCoordinate.longitude &&
    lhs.southwestCoordinate.latitude == rhs.southwestCoordinate.latitude
}

class MapHelper: NSObject {

    static let sharedInstance = MapHelper()

    private override init() { }

}

extension MKMapView {

    func cornersLocation() -> MapCornersLocation {

        let nePoint = CGPointMake(self.bounds.origin.x + self.bounds.size.width, self.bounds.origin.y)
        let swPoint = CGPointMake((self.bounds.origin.x), (self.bounds.origin.y + self.bounds.size.height));

        let neCoordinate = self.convertPoint(nePoint, toCoordinateFromView: self)
        let swCoordinate = self.convertPoint(swPoint, toCoordinateFromView: self)

        return MapCornersLocation(northeastCoorinate: neCoordinate,
                                  southwestCoordinate: swCoordinate)
    }
}
